/* Simple BroadcastEchoServer in GoLang by Phu Phung*/
package main

import (
	"fmt"
	"net"
	"os"
)

const BUFFERSIZE int = 1024

var	allClient_conns = make(map[net.Conn]string)
func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <port>\n", os.Args[0])
		os.Exit(0)
	}
	port := os.Args[1]
	if len(port) > 5 {
		fmt.Println("Invalid port value. Try again!")
		os.Exit(1)
	}
	server, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Printf("Cannot listen on port '" + port + "'!\n")
		os.Exit(2)
	}
	fmt.Println("BroadcastEchoServer in GoLang developed by Phu Phung for concurrent programming hands-on in CPS352")
	fmt.Printf("The server is listening on port '%s' ...\n", port)
	for {
		client_conn, _ := server.Accept()
		fmt.Printf("A new client '%s' connected!\n", client_conn.RemoteAddr().String())
		allClient_conns[client_conn] = client_conn.RemoteAddr().String()
		fmt.Printf("# of connected clients: %d\n", len(allClient_conns))
		welcomemessage := fmt.Sprintf("New client '"+allClient_conns[client_conn]+ "' connected. # of connected clients: %d\n", len(allClient_conns))
		sendtoAll([]byte(welcomemessage))	
		handle_client(client_conn)
	}
}

func handle_client(client_conn net.Conn) {
	var buffer [BUFFERSIZE]byte
	func() {
		for {
			byte_received, read_err := client_conn.Read(buffer[0:])
			if read_err != nil {
				fmt.Println("Error in receiving...")
				return
			}
			fmt.Printf("Received data: %s from '%s'\n",
				buffer[0:byte_received], client_conn.RemoteAddr().String())
			message := client_conn.RemoteAddr().String() + " says:" + string(buffer[0:byte_received])
			sendtoAll([]byte(message))
		}
	}()
}
func sendtoAll(data []byte) {
	for client_conn, _ := range allClient_conns {
		_, write_err := client_conn.Write(data)
		if write_err != nil {
			fmt.Println("Error in sending...")
			return
		}
	}
	fmt.Printf("Sent data: %s to all connected clients\n", data)
}
